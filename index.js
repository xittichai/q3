const request = require('http').request(
    {
        headers: { Cookie: 'hasCookie=true' },
        hostname: 'codequiz.azurewebsites.net',
        method: 'GET',
        path: '/'
    },
    response => {
        let data;
        response.on('data', chunk => (data += chunk));
        response.on('end', () => {
            let nav = data.toUpperCase().replace(/[^A-Z0-9./<>-]/g, '');
            nav = nav.substring(nav.indexOf('CHANGE</TH></TR><TR><TD>') + 24, nav.indexOf('</TD></TR></TABLE>'));
            if (isNaN(process.argv.slice(2)[0]) && nav.indexOf(process.argv.slice(2)[0] + '<') >= 0) {
                nav = nav.substring(nav.indexOf(process.argv.slice(2)[0] + '<')).split('</TD><TD>')[1];
                console.log(nav);
            } else {
                console.log('N/A');
            }
        });
    }
);
request.on('error', error => console.log({ comment: 'Error', error }));
request.end();